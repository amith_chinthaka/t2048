/* The copying permission statement - GNU GPL
 *
 * Author: Amith Chinthaka <mailto:amith@compsoc.lk>
 * March 21, 2014
 * Originally Implemented by Amith Chinthaka based on the amazing 
 * game concept of Game 2048 <http://gabrielecirulli.github.io/2048/>.
 *
 * GameWindow.cpp - UI program of the game
 * This file is part of T2048.
 * 
 * T2048 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * T2048 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with T2048.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<QMessageBox>

#include "gamewindow.h"

const unsigned int ZERO = 0;

enum KeyValue
{
	UP_KEY = 16777235,
	DOWN_KEY = 16777237,
	LEFT_KEY = 16777234,
	RIGHT_KEY = 16777236
};

GameWindow::GameWindow(QWidget *parent, Qt::WFlags flags) : QMainWindow(parent, flags)
{
	ui.setupUi(this);
	
	game_ = new GameAlgorithm;
	GameAlgorithm::Point2DArr board = game_->getInitialBoard();
	updateBoard(board);
}

GameWindow::~GameWindow()
{
}

GameWindow::GameState GameWindow::updateBoard( const GameAlgorithm::Point2DArr& board )
{
	if (board.size() == 16)
	{
		if (board[0].value == NULL) ui.b00->setText("");
		else ui.b00->setNum((int)board[0].value);

		if (board[1].value == NULL) ui.b10->setText("");
		else ui.b10->setNum((int)board[1].value);

		if (board[2].value == NULL) ui.b20->setText("");
		else ui.b20->setNum((int)board[2].value);

		if (board[3].value == NULL) ui.b30->setText("");
		else ui.b30->setNum((int)board[3].value);

		if (board[4].value == NULL) ui.b01->setText("");
		else ui.b01->setNum((int)board[4].value);

		if (board[5].value == NULL) ui.b11->setText("");
		else ui.b11->setNum((int)board[5].value);

		if (board[6].value == NULL) ui.b21->setText("");
		else ui.b21->setNum((int)board[6].value);

		if (board[7].value == NULL) ui.b31->setText("");
		else ui.b31->setNum((int)board[7].value);

		if (board[8].value == NULL) ui.b02->setText("");
		else ui.b02->setNum((int)board[8].value);

		if (board[9].value == NULL) ui.b12->setText("");
		else ui.b12->setNum((int)board[9].value);

		if (board[10].value == NULL) ui.b22->setText("");
		else ui.b22->setNum((int)board[10].value);

		if (board[11].value == NULL) ui.b32->setText("");
		else ui.b32->setNum((int)board[11].value);

		if (board[12].value == NULL) ui.b03->setText("");
		else ui.b03->setNum((int)board[12].value);

		if (board[13].value == NULL) ui.b13->setText("");
		else ui.b13->setNum((int)board[13].value);

		if (board[14].value == NULL) ui.b23->setText("");
		else ui.b23->setNum((int)board[14].value);

		if (board[15].value == NULL) ui.b33->setText("");
		else ui.b33->setNum((int)board[15].value);
	}

	GameWindow::GameState state = check_Win_Loss(board);

	if (state == WIN)
	{
		QMessageBox* box = new QMessageBox();
		box->setWindowTitle(QString("2048"));
		box->setText(QString("Congrats You Won !!!"));
		box->show();
	}

	if (state == LOSS)
	{
		QMessageBox* box = new QMessageBox();
		box->setWindowTitle(QString("2048"));
		box->setText(QString("Sorry You loss :(\nTry Again..."));
		box->show();
	}

	return state;
}

void GameWindow::keyPressEvent( QKeyEvent *e )
{
	GameState state = GOING;

	switch (e->key())
	{
	case UP_KEY:
		state = updateBoard(game_->updateBoard(GameAlgorithm::UP));
		break;
	case DOWN_KEY:
		state = updateBoard(game_->updateBoard(GameAlgorithm::DOWN));
		break;
	case LEFT_KEY:
		state = updateBoard(game_->updateBoard(GameAlgorithm::LEFT));
		break;
	case RIGHT_KEY:
		state = updateBoard(game_->updateBoard(GameAlgorithm::RIGHT));
		break;
	}

	if (state != GOING)
	{
		delete game_;
		game_ = new GameAlgorithm;
		GameAlgorithm::Point2DArr board = game_->getInitialBoard();
		updateBoard(board);
	}
}

GameWindow::GameState GameWindow::check_Win_Loss( const GameAlgorithm::Point2DArr& board )
{
	unsigned int noFreeSlots = 0;
	for (int i = 0; i < board.size(); i++)
	{
		if (board[i].value == ZERO)
		{
			noFreeSlots++;
		}

		if (board[i].value == TARGET)
		{
			return WIN;
		}
	}

	if (noFreeSlots == 0 && !(game_->hasStillAChance()))
	{
		return LOSS;
	}

	return GOING;
}
