/********************************************************************************
** Form generated from reading UI file 'gamewindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEWINDOW_H
#define UI_GAMEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameWindowClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *b00;
    QLabel *b10;
    QLabel *b20;
    QLabel *b30;
    QLabel *b01;
    QLabel *b11;
    QLabel *b21;
    QLabel *b31;
    QLabel *b02;
    QLabel *b12;
    QLabel *b22;
    QLabel *b32;
    QLabel *b03;
    QLabel *b13;
    QLabel *b23;
    QLabel *b33;

    void setupUi(QMainWindow *GameWindowClass)
    {
        if (GameWindowClass->objectName().isEmpty())
            GameWindowClass->setObjectName(QString::fromUtf8("GameWindowClass"));
        GameWindowClass->resize(470, 470);
        GameWindowClass->setMinimumSize(QSize(470, 470));
        GameWindowClass->setMaximumSize(QSize(470, 470));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/GameWindow/T2048.png"), QSize(), QIcon::Normal, QIcon::Off);
        GameWindowClass->setWindowIcon(icon);
        centralWidget = new QWidget(GameWindowClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        b00 = new QLabel(centralWidget);
        b00->setObjectName(QString::fromUtf8("b00"));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft JhengHei"));
        font.setPointSize(26);
        font.setBold(true);
        font.setWeight(75);
        b00->setFont(font);
        b00->setFrameShape(QFrame::Box);
        b00->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b00, 0, 0, 1, 1);

        b10 = new QLabel(centralWidget);
        b10->setObjectName(QString::fromUtf8("b10"));
        b10->setFont(font);
        b10->setFrameShape(QFrame::Box);
        b10->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b10, 0, 1, 1, 1);

        b20 = new QLabel(centralWidget);
        b20->setObjectName(QString::fromUtf8("b20"));
        b20->setFont(font);
        b20->setFrameShape(QFrame::Box);
        b20->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b20, 0, 2, 1, 1);

        b30 = new QLabel(centralWidget);
        b30->setObjectName(QString::fromUtf8("b30"));
        b30->setFont(font);
        b30->setFrameShape(QFrame::Box);
        b30->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b30, 0, 3, 1, 1);

        b01 = new QLabel(centralWidget);
        b01->setObjectName(QString::fromUtf8("b01"));
        b01->setFont(font);
        b01->setFrameShape(QFrame::Box);
        b01->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b01, 1, 0, 1, 1);

        b11 = new QLabel(centralWidget);
        b11->setObjectName(QString::fromUtf8("b11"));
        b11->setFont(font);
        b11->setFrameShape(QFrame::Box);
        b11->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b11, 1, 1, 1, 1);

        b21 = new QLabel(centralWidget);
        b21->setObjectName(QString::fromUtf8("b21"));
        b21->setFont(font);
        b21->setFrameShape(QFrame::Box);
        b21->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b21, 1, 2, 1, 1);

        b31 = new QLabel(centralWidget);
        b31->setObjectName(QString::fromUtf8("b31"));
        b31->setFont(font);
        b31->setFrameShape(QFrame::Box);
        b31->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b31, 1, 3, 1, 1);

        b02 = new QLabel(centralWidget);
        b02->setObjectName(QString::fromUtf8("b02"));
        b02->setFont(font);
        b02->setFrameShape(QFrame::Box);
        b02->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b02, 2, 0, 1, 1);

        b12 = new QLabel(centralWidget);
        b12->setObjectName(QString::fromUtf8("b12"));
        b12->setFont(font);
        b12->setFrameShape(QFrame::Box);
        b12->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b12, 2, 1, 1, 1);

        b22 = new QLabel(centralWidget);
        b22->setObjectName(QString::fromUtf8("b22"));
        b22->setFont(font);
        b22->setFrameShape(QFrame::Box);
        b22->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b22, 2, 2, 1, 1);

        b32 = new QLabel(centralWidget);
        b32->setObjectName(QString::fromUtf8("b32"));
        b32->setFont(font);
        b32->setFrameShape(QFrame::Box);
        b32->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b32, 2, 3, 1, 1);

        b03 = new QLabel(centralWidget);
        b03->setObjectName(QString::fromUtf8("b03"));
        b03->setFont(font);
        b03->setFrameShape(QFrame::Box);
        b03->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b03, 3, 0, 1, 1);

        b13 = new QLabel(centralWidget);
        b13->setObjectName(QString::fromUtf8("b13"));
        b13->setFont(font);
        b13->setFrameShape(QFrame::Box);
        b13->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b13, 3, 1, 1, 1);

        b23 = new QLabel(centralWidget);
        b23->setObjectName(QString::fromUtf8("b23"));
        b23->setFont(font);
        b23->setFrameShape(QFrame::Box);
        b23->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b23, 3, 2, 1, 1);

        b33 = new QLabel(centralWidget);
        b33->setObjectName(QString::fromUtf8("b33"));
        b33->setFont(font);
        b33->setFrameShape(QFrame::Box);
        b33->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(b33, 3, 3, 1, 1);


        verticalLayout->addLayout(gridLayout);

        GameWindowClass->setCentralWidget(centralWidget);

        retranslateUi(GameWindowClass);

        QMetaObject::connectSlotsByName(GameWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *GameWindowClass)
    {
        GameWindowClass->setWindowTitle(QApplication::translate("GameWindowClass", "2048", 0, QApplication::UnicodeUTF8));
        b00->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b10->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b20->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b30->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b01->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b11->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b21->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b31->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b02->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b12->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b22->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b32->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b03->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b13->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b23->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
        b33->setText(QApplication::translate("GameWindowClass", "2", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GameWindowClass: public Ui_GameWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEWINDOW_H
