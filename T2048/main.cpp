/* The copying permission statement - GNU GPL
 *
 * Author: Amith Chinthaka <mailto:amith@compsoc.lk>
 * March 21, 2014
 * Originally Implemented by Amith Chinthaka based on the amazing 
 * game concept of Game 2048 <http://gabrielecirulli.github.io/2048/>.
 *
 * main.cpp - Main program
 * This file is part of T2048.
 * 
 * T2048 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * T2048 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with T2048.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamewindow.h"
#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	GameWindow w;
	w.show();

	return a.exec();
}
