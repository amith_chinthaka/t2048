/* The copying permission statement - GNU GPL
 *
 * Author: Amith Chinthaka <mailto:amith@compsoc.lk>
 * March 21, 2014
 * Originally Implemented by Amith Chinthaka based on the amazing 
 * game concept of Game 2048 <http://gabrielecirulli.github.io/2048/>.
 *
 * GameWindow.h - UI program of the game
 * This file is part of T2048.
 * 
 * T2048 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * T2048 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with T2048.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QtGui/QMainWindow>
#include <QKeyEvent>

#include "ui_gamewindow.h"
#include "GameAlgorithm.h"

class GameWindow : public QMainWindow
{
	Q_OBJECT

public:
	GameWindow(QWidget *parent = 0, Qt::WFlags flags = 0);
	~GameWindow();

private:
	enum GameState
	{
		WIN,
		LOSS,
		GOING
	};

	Ui::GameWindowClass ui;
	GameAlgorithm* game_;

	GameState updateBoard(const GameAlgorithm::Point2DArr& board);
	GameState check_Win_Loss(const GameAlgorithm::Point2DArr& board);

protected:
	void keyPressEvent(QKeyEvent *e);
};

#endif // GAMEWINDOW_H
