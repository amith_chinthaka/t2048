/* The copying permission statement - GNU GPL
 *
 * Author: Amith Chinthaka <mailto:amith@compsoc.lk>
 * March 21, 2014
 * Originally Implemented by Amith Chinthaka based on the amazing 
 * game concept of Game 2048 <http://gabrielecirulli.github.io/2048/>.
 *
 * GameAlgorithm.h - Game Logic
 * This file is part of T2048.
 * 
 * T2048 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * T2048 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with T2048.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GAMEALGORITHM_H
#define GAMEALGORITHM_H

#include <vector>

const int SIDE_LEN	= 4;
const unsigned int TARGET = 2048;

class GameAlgorithm
{
public:
	GameAlgorithm(void);
	~GameAlgorithm(void);

	struct Point2D
	{
		unsigned int x;
		unsigned int y;
		unsigned int value;

		Point2D():x(0), y(0), value(NULL)
		{
		}

		Point2D(unsigned int cx, unsigned int cy, unsigned int v):x(cx), y(cy), value(v)
		{
		}
	};
	typedef std::vector<Point2D> Point2DArr;

	enum Move
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	const Point2DArr getInitialBoard(void);
	const Point2DArr updateBoard(Move move);
	bool hasStillAChance(void);

private:
	unsigned int Board_[SIDE_LEN][SIDE_LEN];

	const int getNextNumberPoint(const Point2DArr& freeSlots);

	void moveUp(void);
	void moveDown(void);
	void moveLeft(void);
	void moveRight(void);

	void mergeUp(void);
	void mergeDown(void);
	void mergerLeft(void);
	void mergeRight(void);

	const unsigned int mergePoints(const unsigned int v1, const unsigned int v2);
};

#endif // GAMEALGORITHM_H