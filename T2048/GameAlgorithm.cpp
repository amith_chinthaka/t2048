/* The copying permission statement - GNU GPL
 *
 * Author: Amith Chinthaka <mailto:amith@compsoc.lk>
 * March 21, 2014
 * Originally Implemented by Amith Chinthaka based on the amazing 
 * game concept of Game 2048 <http://gabrielecirulli.github.io/2048/>.
 *
 * GameAlgorithm.cpp - Game Logic
 * This file is part of T2048.
 * 
 * T2048 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * T2048 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with T2048.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamealgorithm.h"

#include <cstdlib>
#include <ctime>

const unsigned int DEFAULT_VALUE = 2;
const unsigned int ZERO = 0;

GameAlgorithm::GameAlgorithm(void)
{
	Point2DArr freeSlots;
	unsigned int next = 0;

	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			Board_[row][col] = NULL;
			Point2D p2d(row, col, NULL);
			freeSlots.push_back(p2d);
		}
	}

	next = getNextNumberPoint(freeSlots);
	if (next != -1 && next < freeSlots.size())
	{
		Point2D nextNo = freeSlots[next];
		Board_[nextNo.x][nextNo.y] = DEFAULT_VALUE;
		freeSlots.erase(freeSlots.begin() + next);
	}

	next = getNextNumberPoint(freeSlots);
	if (next != -1 && next < freeSlots.size())
	{
		Point2D nextNo = freeSlots[next];
		Board_[nextNo.x][nextNo.y] = DEFAULT_VALUE;
	}
}

GameAlgorithm::~GameAlgorithm(void)
{

}


const GameAlgorithm::Point2DArr GameAlgorithm::getInitialBoard( void )
{
	Point2DArr board;
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			Point2D p2d(row, col, Board_[row][col]);
			board.push_back(p2d);
		}
	}

	return board;
}


const GameAlgorithm::Point2DArr GameAlgorithm::updateBoard(GameAlgorithm::Move move)
{
	switch (move)
	{
	case UP:
		mergeUp();
		moveUp();
		break;
	case DOWN:
		mergeDown();
		moveDown();
		break;
	case LEFT:
		mergerLeft();
		moveLeft();
		break;
	case RIGHT:
		mergeRight();
		moveRight();
		break;
	}

	Point2DArr freeSlots;
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			if (Board_[row][col] == NULL)
			{
				Point2D p2d(row, col, NULL);
				freeSlots.push_back(p2d);
			}
		}
	}

	unsigned int next = getNextNumberPoint(freeSlots);
	if (next != -1 && next < freeSlots.size())
	{
		Point2D nextNo = freeSlots[next];
		Board_[nextNo.x][nextNo.y] = DEFAULT_VALUE;
	}

	Point2DArr board;
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			Point2D p2d(row, col, Board_[row][col]);
			board.push_back(p2d);
		}
	}

	return board;
}

const int GameAlgorithm::getNextNumberPoint(const Point2DArr& freeSlots)
{
	unsigned int range = freeSlots.size();

	if (range == 0)
	{
		return -1;
	}

	std::srand(static_cast<unsigned int>(std::time(NULL)));
	int next = std::rand();

	return next % range;
}

void GameAlgorithm::moveUp(void)
{
	for (int row = 1; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			if (Board_[row][col] != ZERO)
			{
				for (int i = row; i > 0; i--)
				{
					if (Board_[i-1][col] == ZERO)
					{
						Board_[i-1][col] = Board_[i][col];
						Board_[i][col] = ZERO;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
}

void GameAlgorithm::moveDown(void)
{
	for (int row = SIDE_LEN-2; row >= 0; row--)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			if (Board_[row][col] != ZERO)
			{
				for (int i = row; i < SIDE_LEN-1; i++)
				{
					if (Board_[i+1][col] == ZERO)
					{
						Board_[i+1][col] = Board_[i][col];
						Board_[i][col] = ZERO;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
}

void GameAlgorithm::moveLeft(void)
{
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 1; col < SIDE_LEN; col++)
		{
			if (Board_[row][col] != ZERO)
			{
				for (int i = col; i > 0; i--)
				{
					if (Board_[row][i-1] == ZERO)
					{
						Board_[row][i-1] = Board_[row][i];
						Board_[row][i] = ZERO;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
}

void GameAlgorithm::moveRight(void)
{
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = SIDE_LEN-2; col >= 0; col--)
		{
			if (Board_[row][col] != ZERO)
			{
				for (int i = col; i < SIDE_LEN-1; i++)
				{
					if (Board_[row][i+1] == ZERO)
					{
						Board_[row][i+1] = Board_[row][i];
						Board_[row][i] = ZERO;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
}

void GameAlgorithm::mergeUp()
{
	for (int row = 1; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			int val = mergePoints(Board_[row][col], Board_[row-1][col]);
			if (val != ZERO)
			{
				Board_[row-1][col] = val;
				Board_[row][col] = ZERO;
			}
		}
	}
}

void GameAlgorithm::mergeDown()
{
	for (int row = SIDE_LEN-2; row >= 0; row--)
	{
		for (int col = 0; col < SIDE_LEN; col++)
		{
			int val = mergePoints(Board_[row][col], Board_[row+1][col]);
			if (val != ZERO)
			{
				Board_[row+1][col] = val;
				Board_[row][col] = ZERO;
			}
		}
	}
}

void GameAlgorithm::mergerLeft()
{
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 1; col < SIDE_LEN; col++)
		{
			int val = mergePoints(Board_[row][col], Board_[row][col-1]);
			if (val != ZERO)
			{
				Board_[row][col-1] = val;
				Board_[row][col] = ZERO;
			}
		}
	}
}

void GameAlgorithm::mergeRight()
{
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = SIDE_LEN-2; col >= 0; col--)
		{
			int val = mergePoints(Board_[row][col], Board_[row][col+1]);
			if (val != ZERO)
			{
				Board_[row][col+1] = val;
				Board_[row][col] = ZERO;
			}
		}
	}
}

const unsigned int GameAlgorithm::mergePoints(const unsigned int v1, const unsigned int v2)
{
	if (v1 != v2)
	{
		return ZERO;
	}
	else
	{
		return v1 * 2;
	}
}

bool GameAlgorithm::hasStillAChance( void )
{
	for (int row = 0; row < SIDE_LEN; row++)
	{
		for (int col = 0; col < SIDE_LEN-1; col++)
		{
			if (mergePoints(Board_[row][col], Board_[row][col+1]) != ZERO)
			{
				return true;
			}
		}
	}

	for (int col = 0; col < SIDE_LEN; col++)
	{
		for (int row = 0; row < SIDE_LEN-1; row++)
		{
			if (mergePoints(Board_[row][col], Board_[row+1][col]) != ZERO)
			{
				return true;
			}
		}
	}

	return false;
}
